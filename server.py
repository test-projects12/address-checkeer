from flask import render_template, Flask, Response, send_from_directory, request, redirect
import json
import requests
from urllib.parse import unquote

app = Flask(__name__, static_folder='')
app.debug = True

@app.route('/')
def search():
	return app.send_static_file('index.html'), 200

@app.route('/about')
def about():
	return app.send_static_file('about.html'), 200

@app.route('/<filename>')
def staticFile(filename):
	if filename in ['style.css']:
		return app.send_static_file(filename), 200

@app.route('/getData', methods=['POST'])
def getData():
	data = request.form.get('data')
	body = '{"query": "%s", "count": 15}' %(unquote(data))
	body = body.encode(encoding='utf-8')
	r = requests.post('https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address', body, headers={
		"Content-Type": "application/json",
		"Accept": "application/json",
		"Authorization": "Token 49a04b644a76f037e7ea9feb259107f5aab8abe0"
	})
	answer = []
	if r.status_code == 200:
		answer = r.json()
	else:
		print("NOOOOOOO")
		print(r.content)
	resp = Response(json.dumps(answer), status=200, mimetype='application/json')
	resp.headers['Link'] = "locahost:5555"
	return resp

if __name__ == '__main__':
	app.run(host='localhost', port='5555') # ÑÑÐ°ÑÑ ÑÐµÑÐ²ÐµÑÐ°

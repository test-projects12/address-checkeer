#!/bin/sh
ISPIP=$(sudo pip3 -V | wc -c)
if test $ISPIP -eq 0; then
	echo install python3-pip
	sudo apt-get update
	sudo apt-get install python3-pip
fi
ISV=$(sudo pip3 show virtualenv | wc -c)
if test $ISV -eq 0; then
	echo install virtualenv
	pip3 install virtualenv
fi
echo start env
source env/bin/activate
python3 server.py
deactivate
exit